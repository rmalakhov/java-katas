package org.katas.FilmtitelGenerator;

public interface WordChooser {

    /**
     * @param words An array of words
     * @return one word from {@code words}
     */
    public String getWord(String[] words);
}
