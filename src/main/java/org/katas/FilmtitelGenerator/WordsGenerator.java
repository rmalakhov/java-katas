package org.katas.FilmtitelGenerator;

public interface WordsGenerator {

    /**
     * @return An array of words
     */
    public String[] getWords();
}
