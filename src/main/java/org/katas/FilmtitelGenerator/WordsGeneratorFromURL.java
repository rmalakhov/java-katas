package org.katas.FilmtitelGenerator;

public class WordsGeneratorFromURL implements WordsGenerator {

    private final String url;

    public WordsGeneratorFromURL (String url){
        this.url = url;
    }

    @Override
    public String[] getWords() {
        return new String[0];
    }
}
