package org.katas.Fizz;

import org.katas.Kata;

public class Fizz extends Kata {

    @Override
    protected void run() {
        String display = "";

        for (int i = 1; i <= 100; i++) {
            if (i % 5 == 0 && i % 7 == 0) {
                display = "fizzbuzz";
            } else if (i % 5 == 0) {
                display = "fizz";
            } else if (i % 7 == 0) {
                display = "buzz";
            } else {
                display = Integer.toString(i);
            }

            System.out.println(display);
        }
    }
}
