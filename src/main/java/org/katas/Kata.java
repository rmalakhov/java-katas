package org.katas;

public abstract class Kata {
    protected abstract void run();
}
