package org.katas;

import org.katas.Fizz.Fizz;
import org.katas.OddEvent.OddEvent;
import org.katas.Taschenrechner.Calculator;

public class Main {
    public static void main(String[] args) {
//        Kata kataFizz = new Fizz();
//        kataFizz.run();

//        Kata kataOddEvent = new OddEvent();
//        kataOddEvent.run();

        Kata calc = new Calculator();
        calc.run();
    }
}