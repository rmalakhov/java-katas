package org.katas.OddEvent;

import org.katas.Kata;

import java.math.BigInteger;

public class OddEvent extends Kata {

    private String convertToDisplay(char ch) {
        String display = "";

        if (Character.isWhitespace(ch)) {
            display = switch (ch) {
                case '\r' -> "\\r";
                case '\t' -> "\\t";
                case '\n' -> "\\n";
                case '\f' -> "\\f";
                case ' ' -> "space";
                default -> "whitespace";
            };
        } else if (Character.isISOControl(ch)) {
            display = "control";
        } else {
            display = Character.toString(ch);
        }

        return display;
    }
    @Override
    protected void run() {
        String value = "";

        for (int i = 1; i <= 100; i++) {
            BigInteger bigInteger = BigInteger.valueOf(i);
            boolean probablePrime = bigInteger.isProbablePrime((int) Math.log(i));

            if (probablePrime) {
                value = "PRIME";
            } else if (i % 2 == 0) {
                value = "EVEN";
            } else {
                value = "ODD";
            }

            char ch = (char) i;
            System.out.println(i + " - " + value + " (" + this.convertToDisplay(ch) + ")");
        }
    }
}
