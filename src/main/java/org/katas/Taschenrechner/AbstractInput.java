package org.katas.Taschenrechner;

import java.util.Scanner;

public abstract class AbstractInput<V> {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    
    private final String label;
    private final String hint;
    private final String error;

    public AbstractInput(String label, String hint, String error) {
        this.label = label;
        this.hint = hint;
        this.error = error;
    }

    abstract V startInput(Scanner s) throws Exception;

    public V input() {
        V value = null;

        while (value == null) {
            Scanner s = new Scanner(System.in);
            System.out.println(hint);
            System.out.print(label + ": ");

            try {
                value = startInput(s);
            } catch (Exception e) {
                System.out.println(ANSI_RED_BACKGROUND + this.error + ANSI_RESET);
                value = null;
            }
        }

        return value;
    }
}
