package org.katas.Taschenrechner;

import org.katas.Kata;

import java.text.NumberFormat;
import java.util.Random;

public class Calculator extends Kata {
	private static final String ANSI_GREEN = "\033[32m";
	private static final String ANSI_RESET = "\u001B[0m";

	@Override
	protected void run() {		
		OperandOperatorList list = new OperandOperatorList();
		Random rand = new Random();
		
		InputOperator input = new InputOperator("Hi");

		for (int i = 0; i < 7; i++) {
			OperandOperator oo = new OperandOperator(rand.nextDouble());

			Operator operator = Operator.EQUAL;
			if (i < 6) {
				switch (rand.nextInt(4)) {
				case 1: {
					operator = Operator.PLUS;
					break;
				}
				case 2: {
					operator = Operator.MINUS;
					break;
				}
				case 3: {
					operator = Operator.MULTIPLICATION;
					break;
				}
				default: {
					operator = Operator.DIVISION;
					break;
				}
				}
				oo.operator = operator;
			}
			list.add(oo);
		}

		list.display();

		Operation operation = new Operation(list);
		System.out.println(operation.run());

	}
}
