package org.katas.Taschenrechner;

import java.util.Scanner;

public class InputOperand extends AbstractInput<Double> {
    public InputOperand(String label) {
        super(
                label,
                "Please enter the operand value (example: -1,0,1,2,3...)",
                "You entered an invalid value, please see the hint with examples above the input"
        );
    }

    @Override
    protected Double startInput(Scanner s) {
        return s.nextDouble();
    }
}
