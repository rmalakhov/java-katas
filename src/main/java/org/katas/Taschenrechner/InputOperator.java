package org.katas.Taschenrechner;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class InputOperator extends AbstractInput<Character> {	
    public InputOperator() {
        super("Arithmetic operator",
                "Please enter the arithmetic operator: " + Operator.getView(),
                "You entered an invalid operator, please see the hint above the input");
    }

    @Override
    protected Character startInput(Scanner s) throws Exception {
        Character value = s.next().charAt(0);
        
        List<Operator> operators = Arrays.asList(Operator.values());
        if (operators.contains(value)) {
            return value;
        }

        throw new Exception();
    }
}
