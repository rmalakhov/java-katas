package org.katas.Taschenrechner;

import java.text.NumberFormat;

public class OperandOperator {
	public double operand;
	public Operator operator;
    public OperandOperator next;
    public OperandOperator prev;
	
	public OperandOperator(double operand) {
		this.operand = operand;
		this.operator = Operator.EQUAL;
		this.next = null;
		this.prev = null;
	}
	
	public String toString() {
		NumberFormat nf = NumberFormat.getInstance();
		return nf.format(operand) + " " + operator.getMark() + " ";
	}

}
