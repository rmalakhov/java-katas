package org.katas.Taschenrechner;

public class OperandOperatorList {  
    public OperandOperator head = null;    
    public OperandOperator tail = null;
    
    public void add(OperandOperator oo) {    
    	OperandOperator newOo = new OperandOperator(oo.operand);
    	newOo.operator = oo.operator;

        if(head == null) {       
        	head = newOo;    
        	tail = newOo;    
        } else {
        	tail.next = newOo;
        	newOo.prev = tail;
        	tail = newOo;
        }
    }
    
    public void delete(OperandOperator oo) {
    	OperandOperator current = oo;
    	
    	if (current == head) {
    		head = current.next;
    		head.prev = null;
    	} else {
    		current.prev.next = current.next;
    		current.next.prev = current.prev;
    	}
    }
    
    public void display() {
    	OperandOperator current = head; 	
  
    	while(current != null) {
    		System.out.print(current);
    		current = current.next;
    	}
    }
}
