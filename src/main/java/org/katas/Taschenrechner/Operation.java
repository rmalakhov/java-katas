package org.katas.Taschenrechner;

public class Operation {
    OperandOperatorList list;
    
    public Operation(OperandOperatorList list) {
    	this.list = list;
    }

	public double run(Operator operator, double first, double second) {
        switch (operator) {
            case PLUS -> {
                return first + second;
            }
            case MINUS -> {
				return first - second;
            }
            case MULTIPLICATION -> {
                return first * second;
            }
            case DIVISION -> {
                return first / second;
            }
            default -> throw new IllegalArgumentException("Unexpected value: " + operator);
        }
    }
    
    public double run() {
    	OperandOperator current = list.head;
    	
    	boolean h = true;
    	while(current.next != null) {
    		if (current.operator.isHigh()) {
    			calculate(current);
    		} else if (!h) {
    			calculate(current);
    		}
    		
			current = current.next;
			
			if (current.next == null) {
				h = false;
				current = list.head;
			}
    	}
    	
    	return list.head.operand;
    }
    
    private void calculate(OperandOperator oo) {
		oo.next.operand = run(oo.operator, oo.operand, oo.next.operand);
		list.delete(oo);
    }
}






