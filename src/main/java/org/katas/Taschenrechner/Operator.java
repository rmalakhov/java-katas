package org.katas.Taschenrechner;

import java.util.Arrays;
import java.util.Optional;

public enum Operator {
	PLUS('+'),
	MINUS('-'),
	MULTIPLICATION('*'),
	DIVISION('/'),
	EQUAL('=');
	 
    private char mark;
 
    Operator(char mark) {
        this.mark = mark;
    }
 
    public char getMark() {
        return mark;
    }
    
    public boolean isHigh() {
    	Optional<Operator> o = Operator.get(mark);
    	return o.get() == MULTIPLICATION || o.get() == DIVISION;
    }
    
    private static Optional<Operator> get(char mark) {
        return Arrays.stream(Operator.values())
            .filter(o -> o.getMark() == mark)
            .findFirst();
    }
}
